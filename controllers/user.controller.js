const User = require('../models/user.model');
const Joi = require('joi');

//Create and Save a new user
exports.create = async (req,res) => {
    //Validate request
    console.log(req);
    const { error } = validate(req.body);
    if(error) {
        return res.status(400).send(error.details[0].message);
    }

    // Create a new user
    const user = new User({
        username: req.body.username || "Unknown user",
        password: req.body.password ,
        role: req.body.role || "submitter"
    });
    let userOne = await User.findOne({username: req.body.username});
    if(userOne) {
        return res.status(400).send('That user already exists');
    } else{
    user.save()
    .then(data => {
        res.send("User created successfully");
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Errors occured while creating"
        });
    });
    }
}
    //User validation

    function validate(req) {
        const schema = {
            username: Joi.string().min(4).max(255).required(),
            password: Joi.string().min(4).max(255).required(),
            role: Joi.string().min(7).max(15).required()
        };
      
        return Joi.validate(req, schema);
      
};

//Retrieve all users
exports.findAll = (req,res) => {
    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occured while retrieving users"
        });
    });
};
