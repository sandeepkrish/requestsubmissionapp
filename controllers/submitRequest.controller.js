const Request = require('../models/submitRequest.model')
// Create and Save a new Request 

const Joi = require('joi');

exports.create = async (req,res) => {
    // Validate request
    const error = validate(req.body);
    if(error) {
        return res.status(400).send(error.details[0].message);
    }

    //Create a request
    const request = new Request({
        reqTitle: req.body.reqTitle,
        reqDescription: req.body.reqDescription,
        submittedBy: req.body.submittedBy
    });
    let newRequest = await Request.findOne({reqTitle: req.body.reqTitle});
    if(newRequest) {
        return res.status(400).send('Request already exists');
    } else {
        request.save()
        .then(data => {
            res.send("Request created successfully");
        }).catch(err =>{
            res.status(500).send({
                message: err.message || "Errors occured while creating"
            });
        });
    }

};

//Retrieve and return all requests from database
exports.findAll = (req,res) => {
    Request.find()
    .then(request => {
        res.send(request);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occured while retrieving requests"
        });
    });
};

//Find a single Request with requestId 
exports.findOne = (req,res) => {

};

//Update a request with requestId
exports.update = (req,res) => {

};

//Delete a request with requestId
exports.delete = (req,res) => {

};

//Request validation
function validate(req) {
    const schema = {
    reqTitle: Joi.string().min(5).max(100).required(),
    reqDescription: Joi.string().min(5).max(150).required(),
    submittedBy: Joi.string().min(4).max(20).required()
    }
};