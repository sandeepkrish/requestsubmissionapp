const jwt = require('jsonwebtoken');
const Joi = require('joi');
const User = require('../models/user.model');

exports.authenticateUser = async (req,res) => {
  // First Validate The HTTP Request
  const  {error}  = validate(req.body);
  if (error) {
      return res.status(400).send(error.details[0].message);
  }

  //  Now find the user by their email address
  let user = await User.findOne({ username: req.body.username });
  if (!user) {
      return res.status(400).send('Incorrect email or password.');
  }


if(req.body.password === user.password){
  const token = jwt.sign({username:User.username,role:user.role}, 'PrivateKey');
  const role = user.role;
  console.log(user.role)
  res.status(200).send({token,role});
}
else {
    return res.status(400).send('Incorrect  password.');
}
};

function validate(req) {
  const schema = {
      username: Joi.string().min(4).max(255).required(),
      password: Joi.string().min(4).max(255).required()
  };

  return Joi.validate(req, schema);

};