const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 4,
        maxlength: 20,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 4,
        maxlength: 15
    }, 
    role: {
        type: String,
        default: 'submitter',
        enum:["submitter","evaluator","approver"]
    },
    timestamp: { type: Date, default: Date.now},
});


module.exports = mongoose.model('User', UserSchema);



