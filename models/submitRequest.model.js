const mongoose = require('mongoose');

const RequestSchema = mongoose.Schema({
    reqTitle: {
        type: String,
        required: true,
        minlength: 5,
        maxlength:100
    },
    reqDescription: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 150
    },
    submittedBy: {
        type: String,
        required: true,
        minlength:4,
        maxlength:20
    },
    timestamp: {type: Date, default: Date.now},
});
module.exports = mongoose.model('Request',RequestSchema);