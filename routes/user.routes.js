module.exports = (app) => {
    const user = require('../controllers/user.controller');
    const login = require('../controllers/login.controller');

    //Create a new user
    app.post('/api/user', user.create);

    //Retrieve all Users
    app.get('/api/user', user.findAll);

    //Authenticate users
    app.post('/api/login',login.authenticateUser);

}