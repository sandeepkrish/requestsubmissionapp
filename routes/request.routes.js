module.exports = (app) => {
    const submitRequest = require('../controllers/submitRequest.controller');

    // Create new Request
    app.post('/api/request', submitRequest.create);

    // Retrieve all Requests
    app.get('/api/request/showall', submitRequest.findAll)

    // Retrieve a single Request with RequestId
    app.get('/api/request/:requestId', submitRequest.findOne);
    
    //Update a Request with RequestId
    app.put('api/request/:requestId', submitRequest.update);

    //Delete a Request with RequestId
    app.delete('/api/request/:requestId', submitRequest.delete);
}
