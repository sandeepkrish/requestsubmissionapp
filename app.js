const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors')
//create express app
const app = express();

// parse application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


//cors filter
app.use(cors())
//Configure the database
const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() =>{
    console.log("Successfully connected to the database");
}).catch(err =>{
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
})
app.get('/', (req,res) => {
    res.json({"message":"Welcome to the app"})
});
//Import all the routes
require('./routes/user.routes')(app)
require('./routes/request.routes')(app)

app.listen(4000, () =>{
    console.log("Server running on port 3000");
});
